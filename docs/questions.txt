1. No details provided about format of transaction and reconciliation data provided by 3rd parties.
My expectation: all partners sends data in different formats.

2. No details about sensitive and/or GDPR sensitive data provided by 3rd parties.
My expectation: there is no sensitive and protected information, so it is possible to store in DB without modifications.

3. No details about file operations. How it stored, how it updated, naming patterns.
My expectation: CSV like, without nested structures. So, it can be stored to DB as lines of text for further processing.

4. No information about transaction formats.
My expectation: CSV like, without nested structures. So, it can be stored to DB as lines of text for further processing.

5. Not details about Webhook API. Is it supports idempotency.
My expectation: it does not support for idempotency key, so our side should emulate it or use some uniq data from request if it provided. Need more details about transaction content.

6. No information about protocols to interact with microservices.
My expectation: https API

7. No clear understanding of reconciliation process and strategies on data inconsistency.
My expectation: in case of data inconsistency service will schedule execution of data rebuild process and emits event. It can be processed by another component.